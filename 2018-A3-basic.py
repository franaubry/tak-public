from agent import AlphaBetaAgent
import minimax

"""
Agent skeleton. Fill in the gaps.
"""
class MyAgent(AlphaBetaAgent):

  """
  This is the skeleton of an agent to play the Tak game.
  """
  def get_action(self, state, last_action, time_left):
    self.last_action = last_action
    self.time_left = time_left
    return minimax.search(state, self)

  """
  The successors function must return (or yield) a list of
  pairs (a, s) in which a is the action played to reach the
  state s.
  """
  def successors(self, state):
    original_state = state.copy()
    for action in state.get_current_player_actions():
      state = original_state.copy()
      state.apply_action(action)
      yield(action, state)

  """
  The cutoff function returns true if the alpha-beta/minimax
  search has to stop and false otherwise.
  """
  def cutoff(self, state, depth):
    if depth == 1 or state.game_over_check():
      return True
    else:
      return False

  """
  The evaluate function must return an integer value
  representing the utility function of the board.
  """
  def evaluate(self, state):
    ennemy_pieces = 0
    agent_pieces = 0

    if state.is_over()[0] == True:
      if state.is_over()[1] == self.id:
        return 1000
      else:
        return -1000

    for i in range(0, state.size):
      for j in range(0, state.size):
        if state.get_top_piece(i,j) != None:
          if state.get_top_piece(i,j)[0] != 1:
            if state.get_top_piece(i,j)[1] == self.id:
              agent_pieces = agent_pieces + 1
            else:
              ennemy_pieces = ennemy_pieces + 1

    return agent_pieces - ennemy_pieces